import java.util.Scanner;

public class Work_OX
{
  public static Scanner sc = new Scanner(System.in);

  public static void main(String[] args)
  {

    final int SIZE = 3;
    char[][] board = {
      {'-','-','-'},
     {'-','-','-'},
      {'-','-','-'}};

    resetBoard(board);

    System.out.println("Welcome to OX Game");
    showBoard(board);
    char player1 = 'X';
    char player2 = 'O';


    int turn = 1;  // 
    int remainCount = SIZE * SIZE;


    boolean win = false;
    int winner = -1;

    while (!win && remainCount > 0) {
      win = GameWon(board, turn, player1, player2); 

      if (win)
        winner = turn;
      else {

        turn = (turn + 1 ) % 2;

        if (turn == 0)
          PlayerX(board, player1);
        else
          PlayerO(board, player2);

        showBoard(board);
        remainCount--;
      }
    }

    if (winner == 0)
      System.out.println("Player X Win..."+"\nBye bye...");
    else if (winner == 1)
      System.out.println("Player O Win..."+"\nBye bye...");
    else
      System.out.println("** DRAW... **");

  }

  public static void resetBoard(char[][] brd )
  {
    for (int i = 0; i < 3; i++)
      for (int j = 0; j < 3; j++)
        brd[i][j] = '-';
  }

  public static void showBoard(char[][] brd)
  {
    int numRow = 3;
    int numCol = 3;

    System.out.println();

    System.out.println(" 1 2 3");
    for (int i = 0; i < 3; i++) {
      System.out.print(i + 1);
      for (int j = 0; j < 3; j++) {
        System.out.print(brd[i][j] +" ");

        } 
        System.out.println("");
    }
  }

  public static void PlayerX(char[][] brd, char usym)
  {
      System.out.println("X turn");
    System.out.print("\nPlease input Row Col: ");
    int rowIndex = sc.nextInt();
    int colIndex = sc.nextInt();



    brd[rowIndex-1][colIndex-1] = usym;
  }

  public static void PlayerO(char[][] brd, char usym2)
 {
      System.out.println("O turn");
    System.out.print("\nPlease input Row Col: ");
    int rowIndex = sc.nextInt();
    int colIndex = sc.nextInt();
 


    brd[rowIndex-1][colIndex-1] = usym2;
  }

  public static boolean GameWon(char[][] brd, int turn, char play1, char play2)
  {
    char sym;
    if (turn == 0)
      sym = play1;
    else
      sym = play2;

    int i, j;
    boolean win = false;

    for (i = 0; i < 3 && !win; i++) {
      for (j = 0; j < 3; j++) {
        if (brd[i][j] != sym)
          break;
      }
      if (j == 3)
        win = true;
    }

    for (j = 0; j < 3 && !win; j++) {
      for (i = 0; i < 3; i++) {
        if (brd[i][j] != sym)
          break;
      }
      if (i == 3)
        win = true;
    }

    if (!win) {
      for (i = 0; i < 3; i++) {
        if (brd[i][i] != sym)
          break;
      }
      if (i == 3)
        win = true;
    }

    if (!win) {
      for (i = 0; i < 3; i++) {
        if (brd[i][3 - 1 - i] != sym)
          break;
      }
      if (i == 3)
        win = true;
    }
    return win;
  }
}